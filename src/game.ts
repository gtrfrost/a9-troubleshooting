/////////////////////////////////////
// Building in Decentraland course
// Lesson 9 scene to fix
// 
// this scene started out as a DCL 2.2.6, SDK 5.0.0 "dcl init" scene, as follows:
/*
npm rm -g decentraland
npm i -g decentraland@2.2.5
md Lesson9a-SDK5-FixMe
cd Lesson9a-SDK5-FixMe
dcl init
npm rm decentraland-ecs
npm i decentraland-ecs@5.0.0
// imported an SDK5 version of spawnerFunctions.ts module
// got busy reworking the game.ts file to have the basis for the Lesson assignment
*/

import {spawnPlaneX, spawnBoxX, spawnGltfX} from './modules/spawnerFunctions'
import { GroundCover } from './modules/GroundCover'

import {SimpleWaypointSystem, WaypointIdle, WaypointTraveling} from './modules/SimpleWaypointSystem'


const groundMaterial = new Material()
//groundMaterial.albedoColor = new Color3(0.1, 0.4, 0)
groundMaterial.albedoTexture = new Texture('materials/Tileable-Textures/grassy-512-1-0.png')

let ground = new GroundCover(0,0, 16, 0.01, 16, groundMaterial, true)


const box = spawnBoxX(5,0.5,5, 0,0,0,  1,1,1)
const boxMaterial = new Material()
boxMaterial.albedoColor = new Color3(0,.3,0)
boxMaterial.metallic = 1
boxMaterial.roughness = 0
box.addComponent(boxMaterial)


const cube = spawnGltfX(new GLTFShape("models/Lesson9Cube/Lesson9Cube.glb"), 5,1.02,6.01, 180,0,0, 1,1,1)//current

const tree = spawnGltfX(new GLTFShape("models/Tree/tree.gltf"), 8,0,8, 0,0,0,  .3,.3,.3)

const bush = spawnGltfX(new GLTFShape("models/Bush/bush.gltf"), 7,0,5, 0,0,0,  1,1,1)

const firTree = spawnGltfX(new GLTFShape("models/TreeA_Fir/TreeA_Optimised_28_June_2018_A01.babylon.gltf"), 5,0,11, 0,0,0,  .05,.05,.05)

const bird = spawnGltfX(new GLTFShape("models/Sparrow/Sparrow-burung-pipit.gltf"), 5, 1.1,5, 0,0,0, 0.05, 0.05, 0.05)


let animator = new Animator()

bird.addComponent(animator)

const clipFly = new AnimationState("fly")

animator.addClip(clipFly)

clipFly.play()


bird.addComponent(
    new OnClick(e => {
      //log("\n******* bird OnClick *********")
      FlightExample.trigger()
    })
  )

  // Waypoint
const wpSize = 0.001 //!CF todo until such time as the engine.RemoveEntity works, we'll just size them 0.1 m when placing them and 0.001 m when we want them invisible
const wpShape = new GLTFShape("models/xyz/xyzLeftHand.glb")

const waypointEvents = new EventManager() // to handle events from the waypoint system

///////////////
// Waypoint System(s)
// Waypoints
const wp000 = spawnGltfX(wpShape, 5, 1.1,5,   0, 180, 0,   wpSize, wpSize, wpSize)
const wp001 = spawnGltfX(wpShape, 5, 1, 11,   0, 0, 0,   wpSize, wpSize, wpSize) 
const wp002 = spawnGltfX(wpShape, 8, 4.3, 8,   0, 90, 180,   wpSize, wpSize, wpSize)

// Waypoint system that orients the traveler to the next waypoint (y rotation only) at start of flight
const FlightExample = new SimpleWaypointSystem(bird, waypointEvents)
FlightExample.removeWaypointEntities = false // bug in 6.1.3: problems when removing an entity from engine - other object disappear, or some entites lose Tranpose settings
FlightExample.delayTime = 0.5 //secs - any non-zero time will allow haltUntilTriggered to work
FlightExample.travelTime = 2 //secs
FlightExample.haltUntilTriggered = true // touch bird to make it advance
FlightExample.loop = true
FlightExample.orientToNextWaypoint = true
FlightExample.isTraveling = false

FlightExample.pushWaypoint(wp000)
FlightExample.pushWaypoint(wp001)
FlightExample.pushWaypoint(wp002)

// Add listeners that will manage animation start/stop
waypointEvents.addListener(WaypointTraveling, null, ({ /*waypointSystem, eventManager, */traveler }) => {
    if (traveler == bird) {
      //log("\n******* WaypointTraveling event received for bird *********")
      clipFly.play()
    }
    else {
      //log("\n******* WaypointTraveling event received for other *********")
    }
  })
  

  waypointEvents.addListener(WaypointIdle, null, ({ /*waypointSystem, eventManager, */traveler }) => {
    if (traveler == bird) {
      //log("\n******* WaypointTraveling event received for bird *********")
      clipFly.stop()
    }
    else {
      //log("\n******* WaypointTraveling event received for other *********")
    }
  })
 
  engine.addSystem(FlightExample)
